<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
class UserController extends Controller
{
    private $user;


    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function index()
    {   
        $users = $this->user->get();

        return view('admin.users.index',[
            'users' => $users
        ]);
    }


    public function create()
    {
        return view(admin.users.create);
    }


    public function store(Request $request)
    {
        $this->user->create($request->all());
        
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        return view('admin.users.edit',[
            'user' => $this->user->findOrfail($id)
        ]);
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function delete($id)
    {
        $this->user->findOrFail($id)->delete();
        return redirect('/admin/users')->with('alert-type', 'success')
        ->with('alert-message', 'Successfully deleted');
    }
}
