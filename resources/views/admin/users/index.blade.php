@extends('layouts.admin.app')
@section('content')
  @include('layouts.modal')
        <div class="clearfix"></div>
                @include('partials.alert-messages')
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>User <small>Users</small></h2>

                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="table-responsive">
 
                        <table class="table">
                            <thead>
                                <th class="col-md-2">Fullname</th>
                                <th class="col-md-2">Role</th>
                                <th class="col-md-2">Email</th>
                                <th class="col-md-2">Action</th>
                            </thead>
                        <tbody>
                        @foreach ($users as $user)
                        <tr>
                            <td><a href="{{route('admin.users.show',$user->id)}}">{{ $user->fullName }}</a></td>
                            <td>{{ $user->role }}</td>
                            <td>{{ $user->email }}</td>
                            <td>
                                <a class="dropdown-item" href="{{ route('admin.users.edit',$user->id) }}">Update</a>
                                <a class="dropdown-item" href="{{ route('admin.users.delete',$user->id) }}">Delete</a>            
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
                            
                        
                  </div>
                </div>
              </div>
@endsection