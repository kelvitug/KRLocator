@if($errors->all())
    @foreach($errors->all() as $message)
        <div style="padding: 20px;background-color: #f44336;color: white;opacity: 1;transition: opacity 0.6s;margin-bottom: 15px;">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
            {{ $message }}
        </div>
    @endforeach

@elseif(session()->has('message'))
    <div style="padding: 20px;background-color: #4CAF50;color: white;opacity: 1;transition: opacity 0.6s;margin-bottom: 15px;">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
            {{ session()->get('message') }}
    </div>
@elseif(session()->has('error'))
    <div style="padding: 20px;background-color: #f44336;color: white;opacity: 1;transition: opacity 0.6s;margin-bottom: 15px;">
            <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span> 
            {{ session()->get('error') }}
    </div>
@endif



