<!-- Sweet Alert -->
<script type="text/javascript" src="{{asset('gentelella/js/sweetalert.js')}}"></script>
<!-- jQuery -->
<script type="text/javascript" src="{{ asset('gentelella/vendors/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script type="text/javascript" src="{{ asset('gentelella/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script type="text/javascript" src="{{ asset('gentelella/vendors/fastclick/lib/fastclick.js') }}"></script>
<!-- NProgress -->
<script type="text/javascript" src="{{ asset('gentelella/vendors/nprogress/nprogress.js') }}"></script>

<!-- Custom Theme Scripts -->
<script type="text/javascript" src="{{ asset('gentelella/build/js/custom.min.js') }}"></script>

<!-- Chart.js -->
<script type="text/javascript" src="{{ asset('gentelella/vendors/Chart.js/dist/Chart.min.js') }}"></script>
<!-- jQuery Sparklines -->
<script type="text/javascript" src="{{ asset('gentelella/vendors/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- morris.js -->
<script type="text/javascript" src="{{ asset('gentelella/vendors/raphael/raphael.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('gentelella/vendors/morris.js/morris.min.js') }}"></script>
<!-- gauge.js -->
<script type="text/javascript" src="{{ asset('gentelella/vendors/gauge.js/dist/gauge.min.js') }}"></script>
<!-- bootstrap-progressbar -->
<script type="text/javascript" src="{{ asset('gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') }}"></script>
<!-- Skycons -->
<script type="text/javascript" src="{{ asset('gentelella/vendors/skycons/skycons.js') }}"></script>
<!-- Flot -->
<script type="text/javascript" src="{{ asset('gentelella/vendors/Flot/jquery.flot.js') }}"></script>
<script type="text/javascript" src="{{ asset('gentelella/vendors/Flot/jquery.flot.pie.js') }}"></script>
<script type="text/javascript" src="{{ asset('gentelella/vendors/Flot/jquery.flot.time.js') }}"></script>
<script type="text/javascript" src="{{ asset('gentelella/vendors/Flot/jquery.flot.stack.js') }}"></script>
<script type="text/javascript" src="{{ asset('gentelella/vendors/Flot/jquery.flot.resize.js') }}"></script>
<!-- Flot vendors -->
<script type="text/javascript" src="{{ asset('gentelella/vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}"></script>
<script type="text/javascript" src="{{ asset('gentelella/vendors/flot-spline/js/jquery.flot.spline.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('gentelella/vendors/flot.curvedlines/curvedLines.js') }}"></script>
<!-- DateJS -->
<script type="text/javascript" src="{{ asset('gentelella/vendors/DateJS/build/date.js') }}"></script>
<!-- bootstrap-daterangepicker -->
<script type="text/javascript" src="{{ asset('gentelella/vendors/moment/min/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="{{asset('gentelella/vendors/validator/validator.js')}}"></script>

<script type="text/javascript" src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>